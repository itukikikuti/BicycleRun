#include "XLibrary11.hpp"
using namespace XLibrary11;

struct Ground
{
	Float3 position;
	bool enable;

	Ground(float x, float& height)
	{
		height += Random::Range(-8.0f, 8.0f);
		if (height > -100.0f) height = -100.0f;
		if (height < -500.0f) height = -450.0f;
		position.x = x;
		position.y = height;
		enable = Random::GetValue() < 0.99f;
	}
};

int Main()
{
	Camera camera;

	Sprite sprite(L"block.png");
	sprite.color = Float4(0.0f, 0.0f, 0.0f, 1.0f);

	Float3 playerPosition;
	Float3 playerVelocity;
	bool isGround;

	std::vector<Ground> grounds;
	float height;

	bool initFlag = false;

	while (Refresh())
	{
		if (!initFlag)
		{
			playerPosition = Float3(-100.0f, 200.0f, 0.0f);
			isGround = false;
			height = -300.0f;
			for (int i = 0; i < 80; i++)
			{
				grounds.emplace_back(-320.0f + i * 8.0f, height);
			}
			initFlag = true;
		}

		camera.Update();

		height += Random::Range(-8.0f, 8.0f);
		if (height > -100.0f) height = -100.0f;
		if (height < -500.0f) height = -500.0f;
		grounds.emplace_back(320.0f, height);

		for (int i = 0; i < grounds.size(); i++)
		{
			grounds[i].position.x -= 8.0f;

			if (playerPosition.x > grounds[i].position.x - 5.0f &&
				playerPosition.x < grounds[i].position.x + 5.0f &&
				playerPosition.y < grounds[i].position.y + 248.0f)
			{
				playerPosition.y = grounds[i].position.y + 248.0f;
				playerVelocity.y = 0.0f;
				isGround = true;
			}

			if (grounds[i].enable)
			{
				sprite.position = grounds[i].position;
				sprite.scale = Float3(0.5f, 30.0f, 1.0f);
				sprite.Draw();
			}
		}

		playerVelocity.y -= 1.0f;

		if (isGround && Input::GetKeyDown(VK_SPACE))
		{
			playerVelocity.y = 10.0f;
			isGround = false;
		}

		playerPosition += playerVelocity;
		sprite.position = playerPosition;
		sprite.scale = Float3(1.0f, 1.0f, 1.0f);
		sprite.Draw();
	}
}
